
Dell@DESKTOP-USC8MFH MINGW64 ~/Downloads/CTS/stage 3/git-handson
$ git version
git version 2.29.2.windows.2

Dell@DESKTOP-USC8MFH MINGW64 ~/Downloads/CTS/stage 3/git-handson
$ git config --global user.name "Subhajit Laha"

Dell@DESKTOP-USC8MFH MINGW64 ~/Downloads/CTS/stage 3/git-handson
$ git config --global user.email "subhajitlaha.sl@gmail.com"

Dell@DESKTOP-USC8MFH MINGW64 ~/Downloads/CTS/stage 3/git-handson
$ git config --global --list
user.email=subhajitlaha.sl@gmail.com
user.name=Subhajit Laha
filter.lfs.required=true
filter.lfs.clean=git-lfs clean -- %f
filter.lfs.smudge=git-lfs smudge -- %f
filter.lfs.process=git-lfs filter-process
color.ui=auto
core.editor=notepad++.exe -multiInst -nosession

Dell@DESKTOP-USC8MFH MINGW64 ~/Downloads/CTS/stage 3/git-handson
$ notepad++

Dell@DESKTOP-USC8MFH MINGW64 ~/Downloads/CTS/stage 3/git-handson
$ notepad++.exe bash -profile

Dell@DESKTOP-USC8MFH MINGW64 ~/Downloads/CTS/stage 3/git-handson
$ git config --global core.editor "notepad++.exe -multiInst -nosession"

Dell@DESKTOP-USC8MFH MINGW64 ~/Downloads/CTS/stage 3/git-handson
$ git config --global -e

Dell@DESKTOP-USC8MFH MINGW64 ~/Downloads/CTS/stage 3/git-handson
$ git init GitDemo
Initialized empty Git repository in C:/Users/Dell/Downloads/CTS/stage 3/git-handson/GitDemo/.git/

Dell@DESKTOP-USC8MFH MINGW64 ~/Downloads/CTS/stage 3/git-handson
$ ls -al
total 1
drwxr-xr-x 1 Dell 197121  0 Jun 16 11:43 ./
drwxr-xr-x 1 Dell 197121  0 Jun 16 11:34 ../
drwxr-xr-x 1 Dell 197121  0 Jun 16 11:43 GitDemo/
-rw-r--r-- 1 Dell 197121 47 Jun 16 11:41 bash

Dell@DESKTOP-USC8MFH MINGW64 ~/Downloads/CTS/stage 3/git-handson
$ cd GitDemo

Dell@DESKTOP-USC8MFH MINGW64 ~/Downloads/CTS/stage 3/git-handson/GitDemo (master)
$ ls -al
total 4
drwxr-xr-x 1 Dell 197121 0 Jun 16 11:43 ./
drwxr-xr-x 1 Dell 197121 0 Jun 16 11:43 ../
drwxr-xr-x 1 Dell 197121 0 Jun 16 11:43 .git/

Dell@DESKTOP-USC8MFH MINGW64 ~/Downloads/CTS/stage 3/git-handson/GitDemo (master)
$ echo "Welcome to the version control">>welcome.txt

Dell@DESKTOP-USC8MFH MINGW64 ~/Downloads/CTS/stage 3/git-handson/GitDemo (master)
$ ls -al
total 5
drwxr-xr-x 1 Dell 197121  0 Jun 16 11:45 ./
drwxr-xr-x 1 Dell 197121  0 Jun 16 11:43 ../
drwxr-xr-x 1 Dell 197121  0 Jun 16 11:43 .git/
-rw-r--r-- 1 Dell 197121 31 Jun 16 11:45 welcome.txt

Dell@DESKTOP-USC8MFH MINGW64 ~/Downloads/CTS/stage 3/git-handson/GitDemo (master)
$ cat welcme.txt
cat: welcme.txt: No such file or directory

Dell@DESKTOP-USC8MFH MINGW64 ~/Downloads/CTS/stage 3/git-handson/GitDemo (master)
$ cat welcome.txt
Welcome to the version control

Dell@DESKTOP-USC8MFH MINGW64 ~/Downloads/CTS/stage 3/git-handson/GitDemo (master)
$ git status
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        welcome.txt

nothing added to commit but untracked files present (use "git add" to track)

Dell@DESKTOP-USC8MFH MINGW64 ~/Downloads/CTS/stage 3/git-handson/GitDemo (master)
$ git add welcome.txt
warning: LF will be replaced by CRLF in welcome.txt.
The file will have its original line endings in your working directory

Dell@DESKTOP-USC8MFH MINGW64 ~/Downloads/CTS/stage 3/git-handson/GitDemo (master)
$ git commit
[master (root-commit) 48a6da2]  Initial commit
 1 file changed, 1 insertion(+)
 create mode 100644 welcome.txt

Dell@DESKTOP-USC8MFH MINGW64 ~/Downloads/CTS/stage 3/git-handson/GitDemo (master)
$ git status
On branch master
nothing to commit, working tree clean

Dell@DESKTOP-USC8MFH MINGW64 ~/Downloads/CTS/stage 3/git-handson/GitDemo (master)
$ git remote add origin https://github.com/Laha988017/GitDemo.git

Dell@DESKTOP-USC8MFH MINGW64 ~/Downloads/CTS/stage 3/git-handson/GitDemo (master)
$ git push origin master
Enumerating objects: 3, done.
Counting objects: 100% (3/3), done.
Writing objects: 100% (3/3), 254 bytes | 127.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
To https://github.com/Laha988017/GitDemo.git
 * [new branch]      master -> master
